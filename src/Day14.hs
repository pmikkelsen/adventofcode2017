module Day14 where

import Day10
import Day12
import Solution

import Data.Bits
import Data.Char
import Data.Maybe

toBinary :: Int -> [Bool] -- assumes an int is 1 byte
toBinary int = map (testBit int) [7,6..0]

hash :: String -> Int -> [Bool]
hash key row = concatMap toBinary $ knotHash $ key ++ "-" ++ show row

diskState :: String -> [[Bool]]
diskState key = map (hash key) [0..127]

placesUsed :: [[Bool]] -> Int
placesUsed grid = length $ filter id $ concat grid

regions :: [[Bool]] -> Int
regions grid = let usedCells = getUsedCells grid
               in groups $ map (\p -> (p, liveNeighbours usedCells p)) usedCells

getUsedCells :: [[Bool]] -> [(Int,Int)]
getUsedCells grid = [(x,y) | x <- [0..127]
                               , y <- [0..127]
                               , (grid !! y) !! x]

liveNeighbours :: [(Int,Int)] -> (Int, Int) -> [(Int, Int)]
liveNeighbours usedCells (x, y) = filter (`elem` usedCells) [(x+1,y),(x-1,y),(x,y+1),(x,y-1)]

solution input = Solution part1 part2
  where input' = filter (not . isSpace) input
        part1 = placesUsed $ diskState input'
        part2 = regions $ diskState input'
