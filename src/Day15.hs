module Day15 where

import Solution

generatorA1 :: Int -> Int
generatorA1 = generator 16807 1

generatorB1 :: Int -> Int
generatorB1 = generator 48271 1

generatorA2 :: Int -> Int
generatorA2 = generator 16807 4

generatorB2 :: Int -> Int
generatorB2 = generator 48271 8

generator :: Int -> Int -> Int -> Int
generator factor multipleOf start =
  let r = (start * factor) `mod` 2147483647
  in  if r `mod` multipleOf == 0
      then r
      else generator factor multipleOf r

judge :: Int -> (Int -> Int) -> (Int -> Int) -> (Int, Int) -> Int
judge n genA genB (startA, startB) = f n (genA startA, genB startB) 0
  where f 0 _ res = res
        f n (a,b) res =
          let a' = genA a
              b' = genB b
              res' = if a `mod` 65536 == b `mod` 65536
                then res+1
                else res
          in f (n-1) (a',b') res'

readInput :: String -> (Int, Int)
readInput input = let [wordsA, wordsB] = map words $ lines input
                      startA = read $ last wordsA
                      startB = read $ last wordsB
                  in  (startA, startB)


solution input = Solution part1 part2
  where starts = readInput input
        part1 = judge 40000000 generatorA1 generatorB1 starts
        part2 = judge 5000000 generatorA2 generatorB2 starts
