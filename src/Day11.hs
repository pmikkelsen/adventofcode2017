module Day11 where

import Solution
import Data.Char
import Data.List

type Coord = (Int, Int, Int)

data Direction = N
               | NE
               | SE
               | S
               | SW
               | NW
               deriving (Show, Read, Eq)

walk :: Coord -> Direction -> Coord
walk (x,y,z) N = (x+1,y,z)
walk (x,y,z) S = (x-1,y,z)
walk (x,y,z) NE = (x,y+1,z)
walk (x,y,z) SW = (x,y-1,z)
walk (x,y,z) SE = (x,y,z+1)
walk (x,y,z) NW = (z,y,z-1)

minimize :: Coord -> Coord
minimize (x,y,z)
  | x > 0 && z > 0 = (x-1, y+1, z-1)
  | x < 0 && z < 0 = (x+1, y-1, z+1)
  | x > 0 && y < 0 = (x-1, y+1, z-1)
  | x < 0 && y > 0 = (x+1, y-1, z+1)
  | z > 0 && y < 0 = (x-1, y+1, z-1)
  | z < 0 && y > 0 = (x+1, y-1, z+1)
  | otherwise = (x,y,z)

minimize' c = let c' = minimize c
              in  if c' == c
                  then c
                  else minimize' c'

distance :: Coord -> Int
distance (x,y,z) = abs x + abs y + abs z

solution input = Solution (part1 input') (part2 input')
  where input' = map read $ words $ map (\c -> if c == ',' then ' ' else toUpper c) input
        part1 = distance . minimize' . foldl walk (0,0,0)
        part2 = maximum . map part1 . tails . reverse
