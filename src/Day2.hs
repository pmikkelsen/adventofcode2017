module Day2 where

import Solution

solution = generalSolution readInput solve check1 check2
  where readInput = map (map read . words) . lines
        solve check = sum . map check
        check1 xs = maximum xs - minimum xs
        check2 xs = head [z `div` y | z <- xs, y <- xs, z `rem` y == 0, z > y]
