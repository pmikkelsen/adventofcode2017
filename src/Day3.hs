module Day3 where

import Solution
import Data.Maybe

memoryPos = (0,0) : walk (0,0) 0 0 (1,0)
 where walk (x, y) width lastTurn dir@(dx, dy)
         | lastTurn /= width = (x+dx, y+dy) : walk (x+dx, y+dy) width (lastTurn+1) dir
         | lastTurn == width && dir /= (1,0) = walk (x,y) width 0 $ nextDir dir
         | otherwise = (x+dx, y) : walk (x+dx, y) (width+2) 1 (0, 1)
       nextDir (-1,0) = (0,-1)
       nextDir (1,0) = (0,1)
       nextDir (0,1) = (-1,0)
       nextDir (0,-1) = (1,0)

memoryValues = map snd $ ((0,0), 1) : calc [((0,0), 1)] 1
  where calc vals i = getVal vals i : calc (getVal vals i : vals) (i+1)
        getVal values i =
          let pos@(x,y) = memoryPos !! i
              neighbours = [(x+dx, y+dy) | dx <- [-1,0,1], dy <- [-1,0,1]]
              newValue = sum $ mapMaybe (`lookup` values) neighbours
          in  (pos, newValue)

solution = generalSolution read ($) solution1 solution2
  where solution2 input = head $ filter (> input) memoryValues
        solution1 input = dist $ memoryPos !! (input - 1)
        dist (x, y) = abs x + abs y
