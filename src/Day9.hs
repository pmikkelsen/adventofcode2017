module Day9 where

import Solution
import Data.Attoparsec.ByteString.Char8
import Data.ByteString.Char8 (pack)
import Data.Maybe
import Control.Monad

data Group = Group [Group] -- subgroups
           | Garbage Int -- garbage count
  deriving Show

group :: Parser Group
group = do
  char '{'
  subGroups <- sepBy (choice [group, garbage]) (char ',')
  char '}'
  pure $ Group subGroups

garbage :: Parser Group
garbage = do
  char '<'
  garbageCount <- manyTill' garbageContent (char '>')
  pure $ Garbage (sum garbageCount)
  where garbageContent = do
          c <- anyChar
          if c == '!'
            then anyChar >> pure 0
            else pure 1


readInput :: String -> Group
readInput input = let Just res = maybeResult $ parse group $ pack input
                  in res

getScore :: Int -> Group -> Int
getScore level (Group gs) = level + sum (map (getScore (level+1)) gs)
getScore level (Garbage _) = 0

getGarbageCount :: Group -> Int
getGarbageCount (Group gs) = sum $ map getGarbageCount gs
getGarbageCount (Garbage s) = s

solution = generalSolution readInput ($) (getScore 1) getGarbageCount
