module Day7 where

import Solution
import Text.ParserCombinators.ReadP
import Data.List
import Control.Arrow
import Data.Function

data ProgramTree = Tree
  { name :: String
  , weight :: Int
  , subTree :: [ProgramTree]
  } deriving Show

programParser :: ReadP (String, Int, [String])
programParser = do
  name <- many1 (satisfy (/= ' '))
  skipSpaces
  weight <- between (char '(') (char ')') (many1 get)
  subPrograms <- option [] subProgramsParser
  pure (name, read weight, subPrograms)
  where  subProgramsParser = do
          skipSpaces
          string "->"
          skipSpaces
          sepBy1 (many1 (satisfy (/= ','))) (string ", ")

readProgramTree :: String -> ProgramTree
readProgramTree input =
  let programs = readAllPrograms input
  in  head $ toTree programs (roots programs)
  where readAllPrograms = fmap (fst . last . readP_to_S programParser) . lines
        roots ps = filter (\(n,_,_) -> n `notElem` concatMap (\(_,_,s) -> s) ps) ps
        toTree programs = fmap $ \(n,w,s) -> Tree n w (subtree s)
          where subtree s = toTree programs (filter (\(n,_,_) -> n `elem` s) programs)

findCorrection :: ProgramTree -> Int
findCorrection (Tree name _ t) =
  if all isBalanced t
  then let subWeights = map (towerWeight &&& weight) t
           subWeights' = sortOn length $ groupBy ((==) `on` fst) subWeights
           bad = head $ head subWeights'
           good = head $ last subWeights'
           diff = fst good - fst bad
       in  snd bad + diff
  else findCorrection $ head $ filter (not . isBalanced) t
  where towerWeight (Tree _ w t) = w + sum (map towerWeight t)
        isBalanced (Tree _ _ t) = all (== towerWeight (head t)) $ map towerWeight t

solution input = Solution part1 part2
  where tree = readProgramTree input
        part1 = name tree
        part2 = findCorrection tree
