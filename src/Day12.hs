module Day12 where

import Solution
import Data.Attoparsec.ByteString.Char8
import Data.ByteString.Char8 (pack)
import Data.Maybe
import Data.List

parseConnection :: Parser (Int, [Int])
parseConnection = do
  from <- decimal
  skipSpace
  string (pack "<->")
  skipSpace
  to <- sepBy1 decimal (string (pack ", "))
  pure (from, to)

readInput :: String -> [(Int, [Int])]
readInput = mapMaybe (maybeResult . (\x -> feed x (pack "")) . parse parseConnection . pack) . lines

findConnection :: Eq a => [a] -> (a, [a]) -> [a]
findConnection connections (from, to)
  | from `elem` connections = to ++ connections
  | any (`elem` connections) to = from:connections
  | otherwise = connections

connections :: (Eq a, Ord a) => [a] -> [(a, [a])] -> [a]
connections connected pipes =
  let connected' = nub $ foldl findConnection connected pipes
  in  if connected' == connected
      then connected'
      else connections connected' pipes

groups :: (Eq a, Ord a) => [(a, [a])] -> Int
groups pipes = length $ foldl f [] pipes
  where f prevGroups (from,[]) = [from] : prevGroups
        f prevGroups (from,to) = if any (elem from) prevGroups
          then prevGroups
          else connections (from:to) pipes : prevGroups

solution input = Solution (length (connections [0] (readInput input))) (groups (readInput input))
