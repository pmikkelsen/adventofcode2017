module Day4 where

import Solution
import Data.List

solution = generalSolution id countGood check1 check2
  where countGood check = length . filter id . map (check . words) . lines
        check1 = not . any ((<) 1 . length) . group . sort
        check2 = check1 . map sort
