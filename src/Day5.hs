module Day5 where

import Solution

data InstrList = InstrList { left :: [Int]
                           , here :: Int
                           , right :: [Int]}
               deriving Show

-- this is slow but hehe not a real problem.

moveMany :: Int -> InstrList -> InstrList
moveMany n (InstrList ls x rs)
  | n == 0 = InstrList ls x rs
  | n > 0 = InstrList (reverse (take (n-1) rs) ++ (x:ls)) (rs !! (n-1)) (drop n rs)
  | n < 0 = let InstrList rs' x' ls' = moveMany (abs n) $ InstrList rs x ls
            in InstrList ls' x' rs'

move :: Int -> (Int -> Int) -> InstrList -> Int
move count incrFunction (InstrList left offset right)
  | offset > length right = count + 1
  | negate offset > length left = count + 1
  | otherwise = move (count + 1) incrFunction $ moveMany offset (InstrList left (incrFunction offset) right)

solution = generalSolution readInput (move 0) part1 part2
  where readInput input = let x:xs = map read $ lines input
                          in  InstrList [] x xs
        part1 x = x + 1
        part2 x = if x >= 3
                  then x - 1
                  else x + 1
