module Day13 where

import Solution
import Data.List

type Layer = (Int, Int)

readInput :: String -> [Layer]
readInput = map readLayer . lines
  where readLayer str = let (depth, ':':range) = span (/= ':') str
                        in (read depth, read range)

part1 layers = sum $ map f layers
  where f (depth, range) = if depth `mod` (range*2-2) == 0
          then depth * range
          else 0

part2 delay layers =
  if all f layers
  then delay
  else part2 (delay + 1) layers
  where f (depth, range) = ((depth+delay) `mod` (range*2-2)) /= 0

solution = generalSolution readInput ($) part1 (part2 0)
