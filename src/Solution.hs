{-# LANGUAGE ExistentialQuantification #-}

module Solution where

-- A solution is a pair of any two values that can be converted to text.
data Solution = forall a b. (Show a, Show b) => Solution a b

instance Show Solution where
  show (Solution a b) = "Part 1: " ++ show a ++ "\n"
                     ++ "Part 2: " ++ show b ++ "\n"

--                             input-function   function         part1 part2  input
generalSolution :: Show res => (String -> i) -> (a -> i -> res) -> a -> a -> String -> Solution
generalSolution readInput f s1 s2 input = let input' = readInput input
                                          in  Solution (f s1 input') (f s2 input')
