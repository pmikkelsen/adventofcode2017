module Day6 where

import Solution
import Data.List

sol1 :: [[Int]] -> [Int] -> Int
sol1 known banks =
  let banks' = redistribute banks
  in  if banks' `elem` known
      then length known
      else sol1 (banks':known) banks'

sol2 :: [[Int]] -> [Int] -> Int
sol2 known banks =
  let banks' = redistribute banks
  in  if banks' `elem` known
      then let Just first = elemIndex banks' $ reverse known
           in  length known - first
      else sol2 (banks':known) banks'


redistribute :: [Int] -> [Int]
redistribute banks =
  let largest = maximum banks
      Just pos = elemIndex largest banks
      pos' = (pos + 1) `mod` length banks
      banks' = take pos banks ++ [0] ++ drop (pos+1) banks
  in  redistributeHelper pos' largest banks'

redistributeHelper pos 0 banks = banks
redistributeHelper pos count banks =
  let banks' = take pos banks ++ [(banks !! pos) + 1] ++ drop (pos+1) banks
      count' = count - 1
      pos' = if pos == length banks - 1
        then 0
        else pos + 1
  in redistributeHelper pos' count' banks'

solution = generalSolution readInput solve sol1 sol2
  where readInput = map read . words
        solve f banks = f [banks] banks
