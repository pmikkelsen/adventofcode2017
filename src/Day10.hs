module Day10 where

import Solution
import Data.List.Split
import Data.Bits
import Data.Char

newtype Ring a = Ring {ringList :: [a]}
  deriving Show

makeRing :: [a] -> Ring a
makeRing = Ring

move :: Int -> Ring a -> Ring a
move n (Ring lst) = let n' = n `mod` length lst
                    in  Ring $ drop n' lst ++ take n' lst

twist :: Int -> Ring a -> Ring a
twist n (Ring lst) = Ring $ reverse (take n lst) ++ drop n lst

standardRing :: Ring Int
standardRing = makeRing [0..255]

part1 ring lengths = adjust lengths $ foldl f ring $ zip lengths [0..]
  where f ring (length, skip) = move (length + skip) $ twist length ring
        adjust lengths r@(Ring lst) =
          let x = sum (lengths ++ take (length lengths) [0..])
              x' = length lst - x `mod` length lst
          in  move x' r

knotHash input = let input' = map fromEnum input ++ [17,31,73,47,23]
                     lengths = concat $ replicate 64 input'
                     sparsehash = part1 standardRing lengths
                 in  map (foldl1 xor) $ chunksOf 16 $ ringList sparsehash


solution2 = concatMap showAsHex . knotHash

showAsHex :: Int -> String
showAsHex n = let firstDigit = symbols !! (n `div` 16)
                  secondDigit = symbols !! (n `mod` 16)
              in [firstDigit, secondDigit]
  where symbols = "0123456789abcdef"

solution input = Solution solution1 (solution2 input')
  where input' = filter (not . isSpace) input
        input1 = map read $ words $ map (\c -> if c == ',' then ' ' else c) input'
        solution1 = product $ take 2 $ ringList $ part1 standardRing input1
