module Day8 where

import Solution
import Data.Maybe
import Data.List
import Data.Function

data Instruction = AddIf String Int String (Int -> Int -> Bool) Int

readInstruction :: String -> Instruction
readInstruction input =
  let [regA,incOrDec,amount,"if",regB,cmpOp,val] = words input
      amount' = read amount
      val' = read val
      cmpOp' = case cmpOp of
        ">" -> (>)
        "<" -> (<)
        ">=" -> (>=)
        "<=" -> (<=)
        "==" -> (==)
        "!=" -> (/=)
  in case incOrDec of
    "inc" -> AddIf regA amount' regB cmpOp' val'
    "dec" -> AddIf regA (negate amount') regB cmpOp' val'

runProgram :: [Instruction] -> [(String, Int)]
runProgram = foldl runInstruction []
  where runInstruction regs (AddIf regA amount regB cmpOp val) =
          let regBVal = fromMaybe 0 (lookup regB regs)
              regAVal = fromMaybe 0 (lookup regA regs)
          in if regBVal `cmpOp` val
             then (regA, regAVal + amount):regs
             else regs

solution = generalSolution readProgram findLargest (nubBy ((==) `on` fst)) id
  where findLargest f = snd . last . sortOn snd . f . runProgram
        readProgram = map readInstruction . lines
