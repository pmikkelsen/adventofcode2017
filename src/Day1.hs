module Day1 where

import Solution

import Data.List
import Data.Char

solution = generalSolution readInput solve transform1 transform2
  where readInput = map (read . pure) . filter isDigit
        solve transform = sum . concatMap tail . findRepeating . transform
        findRepeating = filter ((<) 1 . length) . group
        transform1 xs = last xs : xs
        transform2 xs =
          let (start, end) = splitAt (length xs `div` 2) xs
              xs' = start ++ reverse end
          in  concat $ zipWith (\x y -> x:y:[10]) xs' $ reverse xs'
