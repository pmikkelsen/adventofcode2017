module Main where

import Solution
import qualified Day1
import qualified Day2
import qualified Day3
import qualified Day4
import qualified Day5
import qualified Day6
import qualified Day7
import qualified Day8
import qualified Day9
import qualified Day10
import qualified Day11
import qualified Day12
import qualified Day13
import qualified Day14
import qualified Day15

solutions :: [(Int, String -> Solution)]
solutions = [ (1, Day1.solution)
            , (2, Day2.solution)
            , (3, Day3.solution)
            , (4, Day4.solution)
            , (5, Day5.solution)
            , (6, Day6.solution)
            , (7, Day7.solution)
            , (8, Day8.solution)
            , (9, Day9.solution)
            , (10, Day10.solution)
            , (11, Day11.solution)
            , (12, Day12.solution)
            , (13, Day13.solution)
            , (14, Day14.solution)
            , (15, Day15.solution)
            ]

printSolution :: Int -> IO ()
printSolution day = do
  putStrLn $ "Solution for day " ++ show day
  case lookup day solutions of
    Nothing -> putStrLn "Sorry, that day has not been solved yet."
    Just solution -> readFile ("inputs/day" ++ show day ++ ".txt") >>= print . solution

main :: IO ()
main = do
  putStrLn "Advent of code, what day do you want the answer from? [1..24] or 'all'"
  day <- getLine
  case day of
    "all" -> mapM_ printSolution [1..25]
    hopefullyANumber -> printSolution $ read hopefullyANumber
